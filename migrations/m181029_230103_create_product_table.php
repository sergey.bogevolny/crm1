<?php
use yii\db\Migration;
/**
* Handles the creation of table `{{%product}}`.
*/
class m181029_230103_create_product_table extends Migration
{
    public $db = 'db';

    public $tableName = '{{%product}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->notNull()->comment('ID'),
            'name_product' => $this->string(500)->null(),
            'price' => $this->money()->null(),
        ]);

        // creates index for column `id`
        $this->createIndex(
            'idx-product-id',
            $this->tableName,
            'id'
        );


    }

    public function safeDown()
    {

        // drop index for column `id`
        $this->dropIndex(
            'idx-product-id',
            $this->tableName
        );

        $this->dropTable($this->tableName);
    }
}

