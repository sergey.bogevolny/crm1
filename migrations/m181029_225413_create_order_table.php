<?php
use yii\db\Migration;
/**
* Handles the creation of table `{{%order}}`.
*/
class m181029_225413_create_order_table extends Migration
{
    public $db = 'db';

    public $tableName = '{{%order}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->notNull()->comment('ID'),
            'date_order' => $this->dateTime()->notNull(),
            'customer_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `id`
      /*   $this->createIndex(
            'idx-order-id',
            $this->tableName,
            'id'
        ); */

        // add foreign key for table `{{%customer}}`
        $this->addForeignKey(
            'fk-order-customer_id-customer-id',
            $this->tableName,
            'customer_id',
            '{{%customer}}',
            'id',            
            'CASCADE'
        );

    }

    public function safeDown()
    {

        // drop index for column `id`
        $this->dropIndex(
            'idx-order-id',
            $this->tableName
        );

        // drop foreign key for table `{{%customer}}`
        $this->dropForeignKey(
            'fk-order-customer_id-customer-id',
            $this->tableName
        );
        $this->dropTable($this->tableName);
    }
}

