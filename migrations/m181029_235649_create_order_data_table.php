<?php
use yii\db\Migration;
/**
* Handles the creation of table `{{%order_data}}`.
*/
class m181029_235649_create_order_data_table extends Migration
{
    public $db = 'db';

    public $tableName = '{{%order_data}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->notNull()->comment('ID'),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'amount' => $this->bigInteger()->notNull(),
            'cost' => $this->money()->null(),
        ]);

        // creates index for column `id`
      /*   $this->createIndex(
            'idx-order_data-id',
            $this->tableName,
            'id'
        ); */

        // add foreign key for table `{{%product}}`
        $this->addForeignKey(
            'fk-order_data-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        // add foreign key for table `{{%order}}`
        $this->addForeignKey(
            'fk-order_data-order_id-order-id',
            $this->tableName,
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE'
        );

    }

    public function safeDown()
    {

        // drop index for column `id`
        $this->dropIndex(
            'idx-order_data-id',
            $this->tableName
        );

        // drop foreign key for table `{{%product}}`
        $this->dropForeignKey(
            'fk-order_data-product_id-product-id',
            $this->tableName
        );
        // drop foreign key for table `{{%order}}`
        $this->dropForeignKey(
            'fk-order_data-order_id-order-id',
            $this->tableName
        );
        $this->dropTable($this->tableName);
    }
}

