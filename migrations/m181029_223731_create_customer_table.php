<?php
use yii\db\Migration;
/**
* Handles the creation of table `{{%customer}}`.
*/
class m181029_223731_create_customer_table extends Migration
{
    public $db = 'db';

    public $tableName = '{{%customer}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->notNull()->comment('ID'),
            'name' => $this->string(100)->notNull(),
            'adress' => $this->string(100)->null(),
        ]);

        // creates index for column `id`
        /* $this->createIndex(
            'idx-customer-id',
            $this->tableName,
            'id'
        ); */


    }

    public function safeDown()
    {

        // drop index for column `id`
        $this->dropIndex(
            'idx-customer-id',
            $this->tableName
        );

        $this->dropTable($this->tableName);
    }
}

