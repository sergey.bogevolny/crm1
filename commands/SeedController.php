<?php
/***
 * Создание заполнения бд
 */

 namespace app\commands;

 use yii\console\Controller;
 use yii\console\ExitCode;
 use app\models\Customer;
 use app\models\Product;
 use app\models\Order;
 use app\models\OrderData;
 

 class SeedController extends Controller
 {
     public function actionIndex()
     {
         $faker = \Faker\Factory::create('ru_RU');

         
        /*  for ($i=1;$i<=300;$i++){
            $customer = new Customer();
            $customer->name = $faker->company;
            $customer->adress = $faker->address;
            $customer->save();
           
        } */
        for ($i=0; $i < 100; $i++) { 
            $order = new Order();
            $order->setIsNewRecord(true);
            $order->id = null;
            $order->date_order = $faker->date;
            $order->customer_id = $faker->numberBetween(0,300);
            if ($order->save()) {
               for ($i=0; $i < 10 ; $i++) { 
                $orderData =  new OrderData();
                $orderData->setIsNewRecord(true);
                $orderData->id = null;
                $orderData->order_id = $order->id;
                $orderData->product_id =$faker->numberBetween(0,200);
                $orderData->amount = $faker->numberBetween(0,1000);
                $orderData->cost = $faker->numberBetween(0,500);
                $orderData->save();
               }
               
            }

        }
 /*        $url = 'http://crm1.loc/yandex.xml';       //адрес YML-файла
 
$yml = simplexml_load_file($url);       //Интерпретирует XML-файл в объект
 //var_dump($yml);
//цикл для обхода по всем товарам
$offer=$yml->shop->offers->offer;

foreach ($offer as $item) {
     $product = new Product();
     $product->name_product=(string)$item->name;
     $product->price=$item->price;
     $product->save();
   
} */
     }
 }