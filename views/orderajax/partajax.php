<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="box-body table-responsive no-padding">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'date_order',
            'name_product',
            'price',
            'amount',
        ]
    ])
    ?>
</div>
