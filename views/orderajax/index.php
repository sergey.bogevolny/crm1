<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'OrdersAJAX';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
    $(document).ready(function () {
            $("tr").on("dblclick", function (ev) {
                var id_order = $("td:first", $(this)).text();
               $("tr").removeClass("highlighted");
              $(this).addClass("highlighted");
                          
                $.ajax({
                    type: "GET",
                    url:"/index.php?r=orderajax/partajax",
                    data: "id_order="+id_order,
                    success: function (msg) {          
                    if ((msg.length>0)){
                        $(".det").html(msg);
                     
                       
                        }
                    }}
                );//  $.ajax({
 
            })//$(\".kv-grid-table tr\").on(\"click\", function (ev) {
        });' //$(document).ready(function () {
        , View::POS_END
);
?>
<style type="text/css"> 

.highlighted { 
    background: grey;
} 
.nonhighlighted { 
    background: white; 
} 
</style> 
<div class="order-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'date_order',
                'customer_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],

            'hover' => true,
            'bordered' => true,
            'striped' => false,
            'condensed' => true,
            'hover' => true,
            'showPageSummary' => false,
            'persistResize' => false,
            'exportConfig' => true,
        ]); ?>
    </div>
    <div class="det">

    </div>
</div>
